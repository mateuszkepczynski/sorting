package sort.strategies;

import sort.AbstractSortStrategy;
import sort.PorownywarkaAlgorytmow;

public class BucketSort extends AbstractSortStrategy {
    public BucketSort() {
        super();
    }

    public void sort() {
        // TODO: sortowanie bucket sort
        int[] bucketArray = new int[PorownywarkaAlgorytmow.PROG_WARTOSCI];
        for (Integer integer : array) {
            bucketArray[integer]++;
        }
        int index=0;
        for (int i = 0; i < PorownywarkaAlgorytmow.PROG_WARTOSCI; i++) {
            for (int j = 0; j < bucketArray[i]; j++) {
                array[index++] = i;
            }
        }
    }
}
