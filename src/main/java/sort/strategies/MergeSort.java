package sort.strategies;

import sort.AbstractSortStrategy;

public class MergeSort extends AbstractSortStrategy {
    public MergeSort() {
        super();
    }

    public void sort() {
        // TODO: sortowanie merge sort
        mergeSort(array,0,array.length-1);
    }

    public void mergeSort(Integer[] tablica,int first, int last){

        if (first != last){
            int middle = ((last-first)/2) + first;
            //split left
            mergeSort(tablica,first,middle);
            //split right
            mergeSort(tablica,middle+1,last);
            //merge tablic
            merge(tablica,first,middle,last);
        }
    }
    public void merge(Integer[] tablica, int first,int middle , int last ){
        Integer[] tmp = new Integer[tablica.length];
        for (int i = 0; i < tablica.length; i++) {
            tmp[i]=tablica[i];
        }
        int i1 = first;
        int i2 = middle+1;
        int index = first;

        while ((i1<=middle)&&(i2<=last)){
            if(tmp[i1]>tmp[i2]){
                tablica[index]=tmp[i2];
                i2++;
            }else {
                tablica[index]=tmp[i1];
                i1++;
            }
            index++;
        }
        for (int i = i1; i <=middle ; i++) {
            tablica[index++]=tmp[i];
        }
        for (int i = i2; i <=last ; i++) {
            tablica[index++]=tmp[i];
        }

    }


}
